const http = require('http');
const todos = [
  {id:1, text: 'Todo One'},
  {id:2, text: 'Todo Two'},
  {id:3, text: 'Todo Three'},
]

const server = http.createServer((req, res) => {
  res.statusCode = 404;
  //res.setHeader('Content-Type', 'text/plain'); // renders plain text response
  //res.setHeader('Content-Type', 'text/html'); // renders html response
  // res.setHeader('Content-Type', 'application/json');
  // res.setHeader('X-Powered-By', 'Node.js');
  // res.setHeader('X-Salutation', 'Happy hacking amigos!');
  
  //res.write('<h1>Hola<h1>');
  //res.write('<h2>Hola Mundo!!</h2>');
  //res.end();
  //res.end(JSON.stringify({
  //  success: true,
  //  data: todos
  // })) //If only one item is being returned it can go inside res.end();
  //JSON.stringify turns the object into a json string

  res.writeHead(400, {
    'Content-Type': 'application/json',
    'X-Powered-By': 'Node.js',
    'X-Salutations': 'Happy hacking amigos!',
  })

  res.end(JSON.stringify({
    success: false,
    error: 'Please add email',
    data: null,
  }))
});

const PORT = 5000;

server.listen(PORT, ()=> {
  console.log(`Server running on port ${PORT}`);
})

