const http = require('http'); /* core node module
 doesn't need to be installed */

const server = http.createServer((req, res)=>{
  //console.log(req); // logs request object
  //console.log(req.method); // pulls method out of req & logs

  const {headers, url, method} = req;
  console.log('-------------------------------');
  console.log('headers object: ', headers);
  console.log('-------------------------------');
  console.log(`url: ${url} | method: ${method}`);
  res.end(); // returns 200 response as long as there are no errors
});

const PORT = 5000;

server.listen(PORT,() => {
  console.log(`Server running on port ${PORT}`);
})