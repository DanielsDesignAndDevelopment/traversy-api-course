const express = require('express');
const app = express();
// Init dotenv & Load environmental variables
const dotenv = require('dotenv').config({ path: './config/config.env' });
const morgan = require('morgan');
const connectDB = require('./config/db.js');
const bootcampsRoutes = require('./routes/bootcamps.js');
const PORT = process.env.PORT;
const colors = require('colors');

// logging middleware
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// Connect to db
connectDB();

// connects url to routes
app.use('/api/v1/bootcamps', bootcampsRoutes);

const server = app.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.italic)
);

process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red.inverse);
  // Close server and exit process
  server.close(()=> process.exit(1));
})