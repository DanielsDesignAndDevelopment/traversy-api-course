// @desc  Logs request to console
exports.logger = (req, res, next) => {
  console.log(`${req.method} ${req.protocol}://${req.get('host')}${req.originalUrl}`);
  next();
}

exports.myMiddleWare = (req, res, next) => {
  req.hello = 'Hola Mundo';
  req.salutations = 'Salut comment allez-vous?';
  console.log(req.hello);
  console.log(req.salutations);
  next();
}
